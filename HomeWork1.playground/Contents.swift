import Foundation

// Task 2
// Create two constants of type Int and print their sum
let termLeft: Int = 5
let termRight: Int = 10

func sum(_ termLeft: Int, _ termRight: Int) -> Int {
    return termLeft + termRight
}

func printSumOfTwoNumbers() {
    print("\(termLeft) + \(termRight) = \(sum(termLeft, termRight))")
}

printSumOfTwoNumbers()


// Task 3
// Create the first constant of type String and insert it into the second variable of type String and print it

let indetifier = UUID.init()
var response = "You have been assigned the following identifier: \(indetifier)"

func showResponse(_ response: String) {
    print(response)
}

showResponse(response)
